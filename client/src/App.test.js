import { render, screen } from '@testing-library/react';
import App from './App';

test('renders learn react link', () => {
  render(<App />);
  const linkElement = screen.getByText(/RS FRONT OPS/i);
  expect(linkElement).toBeInTheDocument();
});
