### Вслучае возникновения проблем с pipelines на gitlab:
- npm install -g npm@latest 
- npm update
- npm outdated
- npm install @testing-library/jest-dom@6.4.5 @testing-library/react@15.0.7 @testing-library/user-event@14.5.2 web-vitals@4.0.1
- npm cache clean --force
- rm -rf node_modules package-lock.json
- npm install